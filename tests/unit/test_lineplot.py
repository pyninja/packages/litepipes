import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter

from litepipes import Sampler
from litepipes.types import UnionDF, PandasDF
from litepipes import TransformerInterface
from litepipes import PlotterInterface
from litepipes import LitePipe


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

data_sampler = Sampler()
TEST_DF = data_sampler(domain='public')


class LinePlotTransformer(TransformerInterface):
    def __init__(self):
        super(LinePlotTransformer, self).__init__()

    def transform(self, df: UnionDF) -> PandasDF:
        local_df = (
            df[df['country'] == 'CZ']
            .groupby(['created_month', 'author_id'])
            .agg(median_interaction=('interactions', 'median'))
            .reset_index()
            .groupby(['created_month'])
            .agg(median_interaction=('median_interaction', 'median'))
            .reset_index()
        )
        local_df['created_month'] = pd.to_datetime(local_df['created_month'])
        return local_df


class LinePlotPlotter(PlotterInterface):
    def __init__(self):
        super(LinePlotPlotter, self).__init__()

    def plot(self, df: UnionDF) -> PandasDF:
        fig, ax = plt.subplots()
        date_format = DateFormatter("%Y %b")
        ax.xaxis.set_major_formatter(date_format)
        ax.plot(df['created_month'], df['median_interaction'])
        # plt.show()
        return df


def test_line_plot():
    """test_line_plot() should test the return dataframe after a in memory plot."""
    pipeline = LitePipe([
        ('line_trf', LinePlotTransformer()),
        ('line_plt', LinePlotPlotter()),
    ])
    temp_df = pipeline.execute(TEST_DF)
    avg_int = temp_df['median_interaction'].mean()
    assert len(temp_df) == 12
    assert avg_int >= 100


