import pandas as pd
import matplotlib.pyplot as plt

from litepipes import Sampler
from litepipes.types import UnionDF, PandasDF
from litepipes import TransformerInterface
from litepipes import PlotterInterface
from litepipes import LitePipe


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

data_sampler = Sampler()
TEST_DF = data_sampler(domain='public')


class BarPlotTransformer(TransformerInterface):
    def __init__(self):
        super(BarPlotTransformer, self).__init__()

    def transform(self, df: UnionDF) -> PandasDF:
        local_df = (
            df[df['created_quarter'] == 'Q4']
            .groupby(['post_type'])
            .agg(median_interaction=('interactions', 'median'))
            .reset_index()
        )
        return local_df


class BarPlotPlotter(PlotterInterface):
    def __init__(self):
        super(BarPlotPlotter, self).__init__()

    def plot(self, df: UnionDF) -> PandasDF:
        fig, ax = plt.subplots()
        ax.bar(df['post_type'], df['median_interaction'])
        # plt.show()
        return df


def test_bar_plot():
    """test_line_plot() should test the return dataframe after a in memory plot."""
    pipeline = LitePipe([
        ('line_trf', BarPlotTransformer()),
        ('line_plt', BarPlotPlotter()),
    ])
    temp_df = pipeline.execute(TEST_DF)
    avg_int = temp_df['median_interaction'].mean()
    assert len(temp_df) <= 3
    assert avg_int >= 100


