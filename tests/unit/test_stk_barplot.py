import pandas as pd
import matplotlib.pyplot as plt
import typing as t
import numpy as np

from litepipes import Sampler
from litepipes.types import UnionDF, PandasDF
from litepipes import TransformerInterface
from litepipes import PlotterInterface
from litepipes import LitePipe


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

data_sampler = Sampler()
TEST_DF = data_sampler(domain='public')


class StackBarPlotTransformer(TransformerInterface):
    def __init__(self):
        super(StackBarPlotTransformer, self).__init__()

    def transform(self, df: UnionDF) -> PandasDF:
        local_df = (
            # Create Your own filter if needed
            df[df['created_quarter'].isin(['Q2', 'Q3', 'Q4'])]
            .groupby(['created_quarter', 'post_type'])
            .agg(median_interaction=('interactions', 'median'))
            .reset_index()
            .pivot(index='created_quarter', columns='post_type', values='median_interaction')
            .reset_index()
        )
        return local_df


class StackBarPlotPlotter(PlotterInterface):
    def __init__(self, col_list: t.List[str], plot_axis: str) -> None:
        self.cols = col_list
        self.x_axis = plot_axis
        super(StackBarPlotPlotter, self).__init__()

    def _create_plot_df(self, df: UnionDF) -> PandasDF:
        totals = df[self.cols].sum(axis=1)
        local_df = pd.DataFrame()
        for col in self.cols:
            local_df[col] = round(df[col] / totals * 100, 2)
        local_df['x_axis'] = df[self.x_axis]
        return local_df

    def plot(self, df: UnionDF) -> PandasDF:
        local_df = self._create_plot_df(df)
        bar_width = 0.85
        names = local_df['x_axis'].tolist()
        fig, ax = plt.subplots()
        bottom = False
        for i, col in enumerate(self.cols):
            rgb = np.random.rand(3, )
            if bottom:
                bottom_cols = self.cols[:i]
                ax.bar(
                    names,
                    local_df[col],
                    bottom=local_df[bottom_cols].sum(axis=1),
                    edgecolor='white',
                    width=bar_width,
                    color=rgb
                )
            else:
                ax.bar(names, local_df[col], edgecolor='white', width=bar_width, color=rgb)
                bottom = True
        # plt.show()
        return local_df


def test_stack_barplot():
    """test_line_plot() should test the return dataframe after a in memory plot."""
    col_list = ['Carousel', 'Photo', 'Video']
    x_axis = 'created_quarter'
    pipeline = LitePipe([
        ('stk_bar_trf', StackBarPlotTransformer()),
        ('stk_bar_plt', StackBarPlotPlotter(col_list=col_list, plot_axis=x_axis)),
    ])
    temp_df = pipeline.execute(TEST_DF)
    assert len(temp_df) == 3


