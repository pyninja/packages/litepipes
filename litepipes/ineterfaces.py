from abc import abstractmethod
from .types import DataHandler, UnionDF, PandasDF


class TransformerInterface(DataHandler):
    """Interface for any Transformer Subclass."""

    @abstractmethod
    def transform(self, df: UnionDF) -> PandasDF:
        """Transform a given dataframe"""
        pass

    def execute(self, df: UnionDF) -> PandasDF:
        """Executor of the Transformer"""
        return self.transform(df)


class PlotterInterface(DataHandler):
    """"Interfaces for plotting subclass"""

    @abstractmethod
    def plot(self, df: UnionDF) -> PandasDF:
        raise NotImplementedError

    def execute(self, df: UnionDF) -> PandasDF:
        return self.plot(df)




