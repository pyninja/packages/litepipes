from .pipelines import Pipeline
from .types import ListPipelines, UnionDF


class Connector:
    """Connector of all pipeline objects"""

    @staticmethod
    def _check_instances(pipes: ListPipelines) -> ListPipelines:
        for item in pipes:
            if not isinstance(item, Pipeline):
                raise ValueError(f"Expected pipeline got {type(item)}")
        return pipes

    def _validate_pipes(self, pipes: ListPipelines) -> ListPipelines:
        pipes = self._check_instances(pipes)
        return pipes

    def __init__(self, pipes: ListPipelines):
        self.pipes = self._validate_pipes(pipes)
        self.new_pipe = self.connect_pipes()

    def connect_pipes(self):
        new_pipeline = []
        for item in self.pipes:
            if item.enable:
                new_pipeline.append(item)
        return new_pipeline

    def execute(self, df: UnionDF, cache=False) -> list:
        cache_store = []
        for pipe in self.pipes:
            result_df = pipe.execute(df)
            if cache:
                pipe_name = str(pipe.__class__.__name__).lower()
                cache_store.append((pipe_name, result_df))
        return cache_store
