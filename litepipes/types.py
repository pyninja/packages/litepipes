from pandas import DataFrame as PandasDF
from pyspark.sql import DataFrame as SparkDF
import typing as t
from abc import ABC, abstractmethod
from inspect import signature


class Pipeline(ABC):
    def __init__(self, enable=True):
        self.enable = enable

    @abstractmethod
    def execute(self, df: t.Union[SparkDF, PandasDF]) -> PandasDF:
        raise NotImplementedError


class DataHandler(ABC):
    """Empty Parent Interface of Quarterly Report"""
    def __init__(self):
        self.info = 'Interface for Quarterly Report'

    @staticmethod
    def _format_repr_args(input_val):
        if isinstance(input_val, str):
            if len(input_val) > 10:
                input_val = '{}...'.format(input_val[:16])
            return "\'{}\'".format(input_val)
        return input_val

    def __repr__(self):
        sig = signature(self.__init__)
        updated_values = self.__dict__
        class_name = self.__class__.__name__
        param_list = []
        for name, _ in sig.parameters.items():
            val = self._format_repr_args(updated_values[name])
            param_list.append('{}={}'.format(name, val))
        param_str = ', '.join(param_list)
        return f'{class_name}({param_str})'


UnionDF = t.Union[PandasDF, SparkDF]
ListPipelines = t.List[Pipeline]
ListTuples = t.List[t.Tuple]
UnionIntStr = t.Union[int, str]

