from .types import Pipeline, ListTuples, UnionDF, PandasDF, UnionIntStr
from .types import DataHandler


class LitePipe(Pipeline):
    @staticmethod
    def _check_step_length(steps: ListTuples) -> ListTuples:
        if len(steps) < 1:
            raise Exception("No steps added in the pipeline!")
        return steps

    @staticmethod
    def _check_duplicate_steps(steps: ListTuples) -> ListTuples:
        duplicates = []
        steps_name = [item[0] for item in steps]
        for name in steps_name:
            if steps_name.count(name) > 1:
                if name not in duplicates:
                    duplicates.append(name)
        if len(duplicates):
            duplicate_str = ','.join(duplicates)
            raise ValueError(f'Duplicates in step name please rename one {duplicate_str}')
        return steps

    @staticmethod
    def _check_instance(steps: ListTuples) -> ListTuples:
        for item in steps:
            if not isinstance(item[1], DataHandler):
                raise ValueError(
                    f'{item[0]} does not satisfy the QuarterlyReportInterface, please implement the abstract methods'
                )
        return steps

    def _validate_steps(self, steps: ListTuples) -> ListTuples:
        steps = self._check_step_length(steps)
        steps = self._check_duplicate_steps(steps)
        return steps

    @staticmethod
    def _create_named_steps(steps: ListTuples):
        steps_dict = dict()
        for item in steps:
            steps_dict[item[0]] = item[1]
        return steps_dict

    def __init__(self, steps: ListTuples, enable=True) -> None:
        self.steps = self._check_step_length(steps)
        self.named_steps = self._create_named_steps(self.steps)
        self.enable = enable
        super(LitePipe, self).__init__(enable=self.enable)

    def __len__(self):
        """Return the length of the Pipeline"""
        return len(self.steps)

    def __getitem__(self, idx: UnionIntStr):
        if isinstance(idx, int):
            max_index = len(self.steps) - 1
            if idx > max_index:
                raise ValueError(f'Given Index is out of range, Maximum Index {max_index}')
        if isinstance(idx, str):
            if idx not in self.named_steps:
                raise ValueError(f'Step {idx} does not exist in the Pipeline')
        try:
            # unpack the name and the object in the tuple of steps
            name,  trf = self.steps[idx]
        except TypeError:
            # Else take the steps from the Ordered dict
            return self.named_steps[idx]
        return trf

    def execute(self, df: UnionDF) -> PandasDF:
        """Executor function to execute all transformer and plotter"""
        print("Process Started!")
        for i, executor in enumerate(self.steps, 1):
            idx, trf = executor
            trf_name = trf.__class__.__name__
            print(f'\t{i} ===> running process {idx} with: {trf_name}()')
            df = trf.execute(df)
        return df

    def __repr__(self):
        own_name = self.__class__.__name__
        all_elements = [item[1].__class__.__name__ for item in self.steps]
        all_elements = ['{}(...)'.format(item) for item in all_elements]
        description = '{}[{}]'.format(own_name, ', '.join(all_elements))
        return description

