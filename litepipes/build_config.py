from pathlib import Path
import typing as t
import typer
import toml
import json
from datetime import date
from litepipes import version


def build_config(rp: Path, wp: Path) -> t.NoReturn:
    config_input = list(rp.glob('*.toml'))
    for path in config_input:
        with open(str(path), 'r') as f:
            obj = toml.load(f)
            obj['updated'] = str(date.today())
            obj['version'] = version
            new_path = wp.joinpath(path.name.replace('.toml', '.json'))
            with open(str(new_path), 'w') as jf:
                json.dump(obj, jf, indent=4)
                print(f'Config created: {str(new_path)}')
    print("Build Complete!")


def main(
        readpath: Path = typer.Option(..., exists=True, file_okay=True, readable=True),
        writepath: Path = typer.Option(..., exists=True, file_okay=True, readable=True)
) -> t.NoReturn:
    build_config(readpath, writepath)


if __name__ == '__main__':
    typer.run(main)
