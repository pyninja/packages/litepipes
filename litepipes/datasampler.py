import pandas as pd
from datetime import date
import numpy as np
import random
import itertools

random.seed(10)


class Sampler:
    def __init__(self):
        self.MEAN_DIST = range(100, 160)
        self.SIGMA = 0.5
        self.COUNTRY = ['US', 'GB', 'CZ']
        self.INDUSTRY = ['Media', 'Auto']
        self.POST_TYPE = ['Photo', 'Video', 'Carousel']
        self.PLATFORM = ['Facebook', 'Instagram', 'Twitter']
        self.START_DATE = date.today().replace(month=1, day=1)
        self.N = 365

    def create_public_sample(self, number_profiles=10):
        data = dict(
            created_date=[], author_id=[], id=[],
            country=[], industry=[], post_type=[],
            platform=[], interactions=[]
        )
        created_date = pd.date_range(start=self.START_DATE, periods=self.N)
        for i in range(1, number_profiles+1):
            data['created_date'].extend(created_date)
            data['author_id'].extend(itertools.repeat(f'profile{i}', self.N))
            data['id'].extend(list(range(1, self.N+1)))
            data['country'].extend(itertools.repeat(np.random.choice(self.COUNTRY), self.N))
            data['industry'].extend(itertools.repeat(np.random.choice(self.INDUSTRY), self.N))
            data['post_type'].extend(itertools.repeat(np.random.choice(self.POST_TYPE), self.N))
            data['platform'].extend(itertools.repeat(np.random.choice(self.PLATFORM), self.N))
            data['interactions'].extend(np.random.normal(np.random.choice(self.MEAN_DIST, 1), self.SIGMA, self.N))
            df = pd.DataFrame(data)
            df['created_month'] = df['created_date'].dt.strftime('%Y-%m-01')
            df['created_quarter'] = df['created_date'].dt.to_period("Q").apply(lambda x: str(x)[-2:])
            df['created_year'] = df['created_date'].dt.year
        return df

    def create_ads_sample(self):
        raise NotImplementedError('Has not been implemented, Please implement a sampler')

    def create_infl_sample(self):
        raise NotImplementedError('Has not been implemented, Please implement a sampler')

    def __call__(self, *args, **kwargs):
        """Call Sample Function based on the sample domain"""
        if kwargs.get('domain') == 'public':
            return self.create_public_sample()
        if kwargs.get('domain') == 'ads':
            return self.create_ads_sample()
        if kwargs.get('domain') == 'influencers':
            return self.create_infl_sample()

        # Default Public Dataframe
        return self.create_public_sample()

    def __repr__(self):
        return '<Sampler: Random Sample Maker>'







