.PHONY: updatecnfs updaterequs testpipes

configs:
	clear
	export PYTHONPATH=.; python ./pkg/config_builder.py \
		--readpath ./tomls \
		--writepath ./meta/configs

requirements:
	clear
	export PYTHONPATH=.; pipreqs . --force

testpipes:
	clear
	export PYTHONPATH=.; pytest ./tests/unit/ | tee ./tests/test.log